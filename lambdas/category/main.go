package main

import (
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/kebabmane/turelo-api/lambdas/repository"
)

// CategoryResponse ...
type CategoryResponse struct {
	Category repository.Category `json:"data"`
}

func handleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	switch req.HTTPMethod {
	case "POST":
		category, err := repository.CreateCategory(req.Body)

		if err != nil {
			return events.APIGatewayProxyResponse{Body: "Could not create category", StatusCode: 404}, nil
		}

		body, err := json.Marshal(CategoryResponse{category})

		if err != nil {
			return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
		}

		return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200,
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			}}, nil

	case "GET":
		category, err := repository.GetCategory(req.PathParameters["id"])

		if err != nil {
			return events.APIGatewayProxyResponse{Body: "Category not found", StatusCode: 404}, nil
		}

		body, err := json.Marshal(CategoryResponse{*category})

		if err != nil {
			return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
		}

		return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200,
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			}}, nil

	}
	return events.APIGatewayProxyResponse{Body: "Unknown error", StatusCode: 500}, nil
}

func main() {
	lambda.Start(handleRequest)
}
