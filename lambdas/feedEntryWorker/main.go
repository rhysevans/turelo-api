package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/service/comprehend"
	strip "github.com/grokify/html-strip-tags-go"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"github.com/kebabmane/turelo-api/lambdas/repository"
	"github.com/mmcdole/gofeed"
)

// Crawl gets the list of feeds, then pumps each feed through to the CrawlFeed function
func Crawl() error {

	ch := make(chan string)

	feeds, err := repository.GetFeeds()
	if err != nil {
		log.Println(err)
	}

	for _, f := range feeds {
		go crawlFeed(f, ch)
	}

	for i := 0; i < len(feeds); i++ {
		fmt.Println(<-ch)
	}
	close(ch)

	return nil
}

// CrawlFeed allows you to crawl feeds
func crawlFeed(f repository.Feed, ch chan<- string) {
	c := &http.Client{
		// give up after 5 seconds
		Timeout: 5 * time.Second,
	}

	fp := gofeed.NewParser()
	fp.Client = c

	feed, err := fp.ParseURL(f.FeedURL)
	if err != nil {
		fmt.Println(err)
		ch <- "failed to fetch and parse for " + f.FeedURL + "\n"
		return
	}

	for _, i := range feed.Items {
		db := repository.GetDB()
		table := os.Getenv("DATABASE_FEED_ENTRIES_TABLE_NAME")
		feedTitleIndex := os.Getenv("DATABASE_FEED_ENTRIES_FEED_TITLE_TABLE_NAME")
		feedEntryTable := db.Table(table)

		var feedEntry *repository.FeedEntry

		err := feedEntryTable.Get("FeedEntryTitle", i.Title).Index(feedTitleIndex).One(&feedEntry)

		if err != nil {
			// generate UUID
			u1 := uuid.NewV4()
			if err != nil {
				fmt.Printf("Something went wrong: %s", err)
			}

			// strip out all the HTML tags
			stripped := strip.StripTags(i.Content)
			localLanguage, err := detectLanguageCode(stripped)

			if err != nil {
				log.Println("[Error] failed to get language code: ", err)
			}

			var feedEntry repository.FeedEntry
			feedEntry.FeedEntryID = u1.String()
			feedEntry.FeedEntryTitle = i.Title
			feedEntry.FeedEntryAuthor = i.Author.Name
			feedEntry.FeedEntryPublished = i.Published
			feedEntry.FeedEntryLink = i.Link
			feedEntry.FeedEntryContent = i.Content
			feedEntry.FeedEntryContentSanitized = stripped
			feedEntry.FeedEntryCreatedAt = time.Now()
			feedEntry.FeedID = f.FeedID
			feedEntry.DetectedLanguage = localLanguage
			b, _ := json.Marshal(feedEntry)
			createFeedEntry(b)
		}
	}
	ch <- "successfully crawled " + f.FeedURL + "\n"
}

// CreateFeedEntry creates a new feed item and returns the []byte json object and an error.
func createFeedEntry(b []byte) {

	var feedEntry repository.FeedEntry
	db := repository.GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_FEED_ENTRIES_TABLE_NAME")
	feedEntryTable := db.Table(table)
	err := json.Unmarshal(b, &feedEntry)

	err = feedEntryTable.Put(&feedEntry).Run()

	if err != nil {
		log.Println(err)
	}
}

func detectLanguageCode(text string) (string, error) {
	input := &comprehend.BatchDetectDominantLanguageInput{}
	log.Println("input text: ", text)
	input.SetTextList([]*string{&text})
	comprehendClient := repository.GetComprehend()
	output, err := comprehendClient.BatchDetectDominantLanguage(input)
	if err != nil {
		log.Println("[Error] failed to aws comprehend detect language: ", err)
		return "", err
	}

	code := ""
	for _, i := range output.ResultList {
		for _, j := range i.Languages {
			if *j.LanguageCode != "" {
				code = *j.LanguageCode
			}
			break
		}
	}
	log.Println("this is the detected language: ", code)
	return code, nil
}

func main() {
	lambda.Start(Crawl)
}
