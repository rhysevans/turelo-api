package main

import (
	"encoding/json"
	"sort"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/kebabmane/turelo-api/lambdas/repository"
)

// AlphabeticalFeedList ...
type AlphabeticalFeedList []repository.Feed

func (a AlphabeticalFeedList) Len() int           { return len(a) }
func (a AlphabeticalFeedList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AlphabeticalFeedList) Less(i, j int) bool { return a[i].FeedName < a[j].FeedName }

// FeedResponse ...
type FeedResponse struct {
	Feeds []repository.Feed `json:"data"`
}

func handleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	list, err := repository.GetFeeds()

	// return here if we get an error
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Feeds not found", StatusCode: 404}, nil
	}

	sort.Sort(AlphabeticalFeedList(list))
	body, err := json.Marshal(FeedResponse{list})

	// if we cannot make the list alphabetical then return an error
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		}}, nil
}

func main() {
	lambda.Start(handleRequest)
}
