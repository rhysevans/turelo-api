package main

import (
	"encoding/json"
	"sort"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/kebabmane/turelo-api/lambdas/repository"
)

// AlphabeticalFeedEntryList ...
type AlphabeticalFeedEntryList []repository.FeedEntry

func (a AlphabeticalFeedEntryList) Len() int      { return len(a) }
func (a AlphabeticalFeedEntryList) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a AlphabeticalFeedEntryList) Less(i, j int) bool {
	return a[i].FeedEntryTitle < a[j].FeedEntryTitle
}

// FeedEntryResponse ...
type FeedEntryResponse struct {
	FeedEntries []repository.FeedEntry `json:"data"`
}

func handleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	feedEntries, err := repository.GetFeedEntries(req.PathParameters["id"])

	// return here if we get an error
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Feed Entries not found", StatusCode: 404}, nil
	}

	sort.Sort(AlphabeticalFeedEntryList(feedEntries))
	body, err := json.Marshal(FeedEntryResponse{feedEntries})

	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		}}, nil
}

func main() {
	lambda.Start(handleRequest)
}
