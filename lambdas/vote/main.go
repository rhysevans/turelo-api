package main

import (
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/kebabmane/turelo-api/lambdas/repository"
)

// FeedEntryVoteResponse ...
type FeedEntryVoteResponse struct {
	FeedEntryVote int `json:"voteCount"`
}

func handleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	vote, err := repository.FeedEntryVote(req.PathParameters["FeedID"], req.PathParameters["vote"])

	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Could not vote", StatusCode: 404}, nil
	}

	body, err := json.Marshal(FeedEntryVoteResponse{vote})

	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200}, nil
}

func main() {
	lambda.Start(handleRequest)
}
