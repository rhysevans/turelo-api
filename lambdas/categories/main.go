package main

import (
	"encoding/json"
	"sort"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/kebabmane/turelo-api/lambdas/repository"
)

// AlphabeticalCategoryList ...
type AlphabeticalCategoryList []repository.Category

func (a AlphabeticalCategoryList) Len() int           { return len(a) }
func (a AlphabeticalCategoryList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AlphabeticalCategoryList) Less(i, j int) bool { return a[i].CategoryName < a[j].CategoryName }

// CategoryResponse ...
type CategoryResponse struct {
	Categories []repository.Category `json:"catagories"`
}

func handleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	list, err := repository.GetCategories()

	// return here if we get an error
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Categories not found", StatusCode: 404}, nil
	}

	sort.Sort(AlphabeticalCategoryList(list))
	body, err := json.Marshal(CategoryResponse{list})

	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		}}, nil
}

func main() {
	lambda.Start(handleRequest)
}
