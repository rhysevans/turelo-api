package main

import (
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/kebabmane/turelo-api/lambdas/repository"
)

// FeedEntryResponse ...
type FeedEntryResponse struct {
	FeedEntry repository.FeedEntry `json:"data"`
}

func handleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	feedEntry, err := repository.GetFeedEntry(req.PathParameters["id"])

	// return here if we get an error
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Feed Entry not found", StatusCode: 404}, nil
	}

	body, err := json.Marshal(FeedEntryResponse{*feedEntry})

	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		}}, nil
}

func main() {
	lambda.Start(handleRequest)
}
