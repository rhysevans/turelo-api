package repository

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

// Category data model
type Category struct {
	CategoryName        string `dynamo:"CategoryName"`
	CategoryImageURL    string `dynamo:"DatabaseImageURL"`
	CategoryDescription string `dynamo:"CategoryDescription"`
	FeedsCount          string `dynamo:"FeedsCount"`
	CategoryID          string // Hash key
	CreatedAt           time.Time
}

// Feed data model
type Feed struct {
	FeedID          string
	FeedName        string `dynamo:"FeedName"`
	FeedURL         string `dynamo:"FeedURL"`
	FeedIcon        string `dynamo:"FeedIcon"`
	FeedsCount      string `dynamo:"FeedsCount"`
	LastFeteched    string `dynamo:"LastFetched"`
	FeedDescription string `dynamo:"FeedDescriptiom"`
	FeedImageURL    string `dynamo:"FeedImageURL"`
	FeedLastUpdated time.Time
	Categories      []Category
	FeedEntry       []FeedEntry
}

// FeedEntry data model
type FeedEntry struct {
	FeedEntryID               string
	FeedEntryTitle            string `dynamo:"FeedEntryTitle"`
	FeedEntryURL              string `dynamo:"FeedEntryURL"`
	FeedEntryPublished        string `dynamo:"FeedEntryPublished"`
	FeedEntryCreatedAt        time.Time
	FeedEntryAuthor           string `dynamo:"FeedEntryAuthor"`
	FeedEntryContent          string `dynamo:"FeedEntryContent"`
	FeedEntryContentSanitized string `dynamo:"FeedEntryContentSanitized"`
	FeedEntryLink             string `dynamo:"FeedEntryLink"`
	FeedID                    string
	FeedEntryVote             int
	FeedEntryViews            int
	DetectedLanguage          string
	// Entities                  []Entity    `dynamo:",set"`
	// KeyPhrases                []Phrase    `dynamo:",set"`
	// Sentiment                 []Sentiment `dynamo:",set"`
}

// Entity struct...
type Entity struct {
	BeginOffset int    `dynamo:"BeginOffset"`
	EndOffset   int    `dynamo:"EndOffset"`
	Score       string `dynamo:"Score"`
	Text        string `dynamo:"Text"`
	Type        string `dynamo:"Type"`
}

// Phrase struct
type Phrase struct {
}

type Sentiment struct {
}

// GetFeed does...
func GetFeed(id string) (*Feed, error) {

	var feed *Feed
	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_FEEDS_TABLE_NAME")
	feedTable := db.Table(table)

	err := feedTable.Get("FeedID", id).One(&feed)

	if feed == nil {
		err := errors.New("Not found")
		return nil, err
	}

	return feed, err
}

// GetFeeds does...
func GetFeeds() ([]Feed, error) {

	var feeds []Feed
	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_FEEDS_TABLE_NAME")
	feedTable := db.Table(table)

	err := feedTable.Scan().All(&feeds)

	if len(feeds) == 0 {
		err := errors.New("Not found")
		{
			log.Println("you are returning the error")
			return nil, err
		}
	}

	return feeds, err
}

// GetCategory does...
func GetCategory(id string) (*Category, error) {

	var category *Category
	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_CATEGORIES_TABLE_NAME")
	categoryTable := db.Table(table)

	err := categoryTable.Get("CategoryID", id).One(&category)

	if category == nil {
		err := errors.New("Not found")
		return nil, err
	}

	return category, err
}

// CreateCategory does...
func CreateCategory(body string) (Category, error) {

	var err error
	var category Category

	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_CATEGORIES_TABLE_NAME")
	categoryTable := db.Table(table)

	// take whats in the
	err = json.Unmarshal([]byte(body), &category)
	if err != nil {
		log.Println(err.Error())
	}

	if category.CategoryName == "" {
		// if category name is a nil string then return with an error
		return category, errors.New("Category name must be set")
	}

	err = categoryTable.Get("CategoryName", category.CategoryName).Index(os.Getenv("DATABASE_CATEGORIES_NAME_TABLE_NAME")).One(&category)

	if err != nil {
		// generate UUID
		u1 := uuid.NewV4()
		if err != nil {
			fmt.Printf("Something went wrong: %s", err)
		}

		category.CategoryID = u1.String()
		category.CreatedAt = time.Now()

		// marshal the data
		b, _ := json.Marshal(category)

		// goahead and create the new category
		err = createCategory(b)
		if err != nil {
			log.Println(err.Error())
		}
	}

	return category, err
}

// createCategory creates a new feed item and returns the []byte json object and an error.
func createCategory(b []byte) error {

	var category Category
	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_CATEGORIES_TABLE_NAME")
	categoryTable := db.Table(table)
	err := json.Unmarshal(b, &category)

	err = categoryTable.Put(&category).Run()

	if err != nil {
		log.Println(err)
	}
	return err
}

// GetCategories does...
func GetCategories() ([]Category, error) {

	var categories []Category
	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_CATEGORIES_TABLE_NAME")
	categoryTable := db.Table(table)

	err := categoryTable.Scan().All(&categories)

	log.Println("categories error: ", err)

	log.Println("categories response: ", categories)

	if len(categories) <= 0 {
		err := errors.New("Not found")
		{
			return nil, err
		}
	}

	return categories, err
}

// GetFeedEntry does...
func GetFeedEntry(id string) (*FeedEntry, error) {

	var feedEntry *FeedEntry
	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_FEED_ENTRIES_TABLE_NAME")
	feedEntryTable := db.Table(table)

	err := feedEntryTable.Get("FeedEntryID", id).One(&feedEntry)

	if feedEntry == nil {
		err := errors.New("Not found")
		return nil, err
	}

	// all we really want here is the int number of views
	views := feedEntry.FeedEntryViews

	// increment the views by 1
	views++

	// you aren't doing anything with this error idiot
	err = feedEntryTable.Update("FeedEntryID", id).Set("FeedEntryViews", views).Run()

	return feedEntry, err
}

// GetFeedEntries does...
func GetFeedEntries(id string) ([]FeedEntry, error) {

	var feedEntries []FeedEntry
	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_FEED_ENTRIES_TABLE_NAME")
	feedEntryTable := db.Table(table)

	// limit our result set to 10 items, this should return the page key
	err := feedEntryTable.Scan().Filter("'FeedID' = ?", id).SearchLimit(10).All(&feedEntries)

	if len(feedEntries) == 0 {
		err := errors.New("Not found")
		{
			log.Println("you are returning the error")
			log.Println(err)
			return nil, err
		}
	}

	return feedEntries, err
}

// FeedEntryVote does...
func FeedEntryVote(id string, state string) (int, error) {

	var feedEntry *FeedEntry
	db := GetDB()
	// get the table from OS vars
	table := os.Getenv("DATABASE_FEED_ENTRIES_TABLE_NAME")
	feedEntryTable := db.Table(table)

	err := feedEntryTable.Get("FeedEntryID", id).One(&feedEntry)

	// all we really want here is the int number of votes
	vote := feedEntry.FeedEntryVote

	i, err := strconv.Atoi(state)

	if i == 1 {
		vote++
	}
	if i == -1 {
		vote--
	}

	// you aren't doing anything with this error idiot
	err = feedEntryTable.Update("FeedEntryID", id).Set("FeedEntryVote", vote).Run()

	return vote, err
}
