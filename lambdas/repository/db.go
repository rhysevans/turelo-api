package repository

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/comprehend"
	"github.com/guregu/dynamo"
)

var db *dynamo.DB

// GetDB does stuff
func GetDB() *dynamo.DB {
	db := dynamo.New(session.New(), &aws.Config{Region: aws.String("us-east-1")})
	return db
}

// Setup a Comprehend Session
func GetComprehend() *comprehend.Comprehend {
	sess := session.Must(session.NewSession())
	client := comprehend.New(sess, aws.NewConfig().WithRegion("us-west-2"))
	return client
}
