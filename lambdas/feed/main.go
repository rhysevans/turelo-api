package main

import (
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/kebabmane/turelo-api/lambdas/repository"
)

// FeedResponse ...
type FeedResponse struct {
	Feed repository.Feed `json:"data"`
}

func handleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	feed, err := repository.GetFeed(req.PathParameters["id"])

	// return here if we get an error
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Feed not found", StatusCode: 404}, nil
	}

	body, err := json.Marshal(FeedResponse{*feed})

	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		}}, nil
}

func main() {
	lambda.Start(handleRequest)
}
