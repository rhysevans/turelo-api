build:
	rm -rf bin/*
	dep ensure
	env GOOS=linux go build -ldflags="-s -w" -o bin/feed lambdas/feed/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/feeds lambdas/feeds/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/feedEntry lambdas/feedEntry/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/feedEntries lambdas/feedEntries/main.go	
	env GOOS=linux go build -ldflags="-s -w" -o bin/category lambdas/category/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/categories lambdas/categories/main.go	
	env GOOS=linux go build -ldflags="-s -w" -o bin/feedEntryWorker lambdas/feedEntryWorker/main.go	
	env GOOS=linux go build -ldflags="-s -w" -o bin/vote lambdas/vote/main.go	
